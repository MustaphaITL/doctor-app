import 'package:doctor_app/pages/home.dart';
import 'package:doctor_app/pages/wrapper.dart';
import 'package:doctor_app/screens/sign_in.dart';
import 'package:doctor_app/theme/colors.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: primary,
        ),
        //Home first
        home: Wrapper());
  }
}
