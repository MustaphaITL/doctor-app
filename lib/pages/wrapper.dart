import 'package:doctor_app/models/User.dart';
import 'package:doctor_app/pages/home.dart';
import 'package:doctor_app/services/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:doctor_app/screens/authentication.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  AuthService _auth = new AuthService();
  @override
  Widget build(BuildContext context) {
    // return either the Home or Authenticate widget
    final user =
        _auth.signInWithEmailAndPassword('test@mail.com', 'somepasword12345');
    // return either the Home or Authenticate widget
    if (user != null) {
      return Authenticate();
    } else {
      return Home();
    }
  }
}
